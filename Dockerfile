FROM debian:bookworm
RUN apt-get update && apt-get install -qy auto-apt-proxy
RUN apt-get update && apt-get install -qy ruby puma ruby-sinatra ruby-public-suffix
COPY ./ /app
WORKDIR /app
USER www-data
ENV LANG=C.UTF-8
CMD puma --bind tcp://0.0.0.0:${PORT:-5000}
