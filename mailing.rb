require "digest/sha1"
require "pathname"
require "public_suffix"
require "sinatra"
require "uri"

set :data_dir, ENV["APP_DATA_DIR"] || Dir.mktmpdir("mailing-")
set :redirect, ENV["REDIRECT"] || "/"
set :home_redirect, ENV["HOME_REDIRECT"] || "/"

get "/" do
  if settings.home_redirect =~ /https?:\/\//
    redirect settings.home_redirect
  else
    "Hello, world"
  end
end

post "/" do
  return 200 if params[:name] && !params[:name].empty?

  email = params[:email]
  return 400 unless email
  return 400 if email.empty?
  return 400 unless email =~ URI::MailTo::EMAIL_REGEXP
  parts = email.split("@")
  domain = parts.last
  return 400 unless PublicSuffix.valid?(domain, default_rule: nil)

  hash = Digest::SHA1.hexdigest(email)
  path = Pathname.new(settings.data_dir) / hash
  path.write(email + "\n")
  redirect settings.redirect
end

error 400 do
  "Invalid email address"
end
