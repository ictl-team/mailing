ENV['RACK_ENV'] = "test"
$LOAD_PATH << '.'

if ENV["COVERAGE"] != "no"
  begin
    require 'simplecov'
    SimpleCov.start do
      minimum_coverage(100)
      track_files('mailing.rb')
    end
  rescue LoadError
    $stderr.puts "W: simplecov not installed, not checking test coverage"
  end
end

require 'rack/test'
require 'mailing'

describe "mailing" do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  let(:tmpdir) do
    `mktemp --directory`.strip
  end

  let(:emails) do
    Dir.glob(tmpdir + "/*")
  end

  before(:each) do
    app.set :data_dir, tmpdir
  end

  after(:each) do
    FileUtils::rm_rf(tmpdir)
  end

  it "serves a basic page on GET" do
    get "/"
    expect(last_response.status).to eq(200)
  end

  it "redirects / to configured HOME_REDIRECT" do
    app.set :home_redirect, "https://example.com/"
    get "/"
    expect(last_response.status).to eq(302)
    expect(last_response.location).to eq("https://example.com/")
  end

  it "discards any request with a name" do
    post "/", name: "foo bar", email: "foo@example.com"
    expect(last_response.status).to eq(200)
    expect(emails.count).to eq(0)
  end

  it "accepts request with an empty name" do
    post "/", name: "", email: "foo@example.com"
    expect(last_response.status).to eq(302)
    expect(emails.count).to eq(1)
  end

  it "saves valid emails" do
    post "/", email: "foo@example.com"
    expect(last_response.status).to eq(302)
    expect(emails.count).to eq(1)
  end

  it "rejects empty POSTS" do
    post "/"
    expect(last_response.status).to eq(400)
    expect(emails.count).to eq(0)
  end

  [
    "",
    "  ",
    "foo@example.com   ",
    "foo-example.com",
    "foo@example.nope",
    "foo@foo@example.com",
  ].each do |invalid|
    it "rejects #{invalid.inspect} as an invalid email" do
      post "/", email: invalid
      expect(last_response.status).to eq(400)
      expect(emails.count).to eq(0)
    end
  end
end
